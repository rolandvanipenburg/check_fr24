# NAME

check\_fr24 - check the status of a flightradar24 feeder

# USAGE

**check\_fr24** **-H** _host_ **-p** _property_

# DESCRIPTION

**check\_fr24** checks the status of several properties of a flightradar24
feeder, similar to what _fr24feed-status_ shows.

# REQUIRED ARGUMENTS

- **-p, --property**

    Check for one of the following properties:

    - _link_: Connected or not
    - _mlat_: MLAT running or not
    - _offline_: Offline mode or not
    - _receiver_: Connected or not
    - _shutdown_: Shutdown or not
    - _timing_: Timing last result success
    - _tracked_: Aircraft Tracked: (ModeS & ADS-B), default doesn't warn
    when this is zero because that value can be a correct representation.
    - _version_: System software version, default warns if not the same as
    the given release or the default API version _1.0.24-5_.

# OPTIONS

- **-H, --host**

    The host to get the data from. Defaults to _localhost_.

- **-r, --release**

    To suppress the warning of the version check after a newer release of the
    software is installed but the API hasn't changed. This avoids having to update
    this script with every release just to update the release number.

- **-m, --max\_age**

    The maximum cache age in seconds used for the retrieved data. This is meant to
    be used with a small value so checks for multiple properties in the same
    monitoring cycle can share a request. It doesn't make sense to use values that
    exceed the monitoring interval duration, then the monitoring interval should
    just be increased.

- **-?, -h, --help**

    Show help and license

- **-v, --verbose**

    Be more verbose

- **--version**

    Show version

# DIAGNOSTICS

- _Could not retrieve resource '%s' to get data_

    (E) The resource containing the data could not be retrieved from the host

- _Could not get data from resource '%s' for property '%s'_

    (E) The resource containing the data could be retrieved from the host but the
    requested data was not found in that object

- _Unknown property '%s', must be one of link, mlat, offline,
receiver, shutdown, timing, tracked, version_

    (E) The requested property is not supported, must be one of the properties
    'link', 'mlat', 'offline', 'receiver', 'shutdown', 'timing', 'tracked' or 'version'.

- _Can't check alias '%s' and property '%s' at once_

    (E) Checking both the alias and a property in the same command would yield
    conflicting results.

# EXAMPLES

`check_fr24 -H fr24.local -p link`

# DEPENDENCIES

Perl 5.16.0, Monitoring::Plugin, HTTP::Tiny::Cache, JSON, Readonly

# EXIT STATUS

The exit status is handled by the monitoring interface.

# CONFIGURATION

The environment variable _HTTP\_TINY\_CACHE\_MAX\_AGE_ is used to set the default
caching period for requests to 60 seconds if it wasn't set already, or to the
value given by the _max\_age_ option.

# INCOMPATIBILITIES

There are no known incompatibilities but since the data is retrieved from an
API in an undefined format, a different system software version could present
the data in a different format which might break things. It is compatible with
feeder version _1.0.24-5_.

# BUGS AND LIMITATIONS

The 'link', 'receiver', 'shutdown' and 'timing' properties only determine
between 'success'/'CONNECTED'/'YES'/'no' and their opposites.

Please report any bugs or feature requests at
[Bitbucket](https://bitbucket.org/rolandvanipenburg/check_fr24/issues).

# AUTHOR

Roland van Ipenburg, <roland@rolandvanipenburg.com>

# LICENSE AND COPYRIGHT

Copyright 2020 by Roland van Ipenburg
This program is free software; you can redistribute it and/or modify
it under the GNU General Public License v3.0.

# DISCLAIMER OF WARRANTY

BECAUSE THIS SOFTWARE IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE SOFTWARE, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE
ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH
YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL
NECESSARY SERVICING, REPAIR, OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE SOFTWARE AS PERMITTED BY THE ABOVE LICENSE, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL,
OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE
THE SOFTWARE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE SOFTWARE TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.
